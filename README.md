# CustomersFX MVC Application Demo Project

![](screenshots/101-customers-page.png)

Technology stack:
--
- Java 11
- JavaFX 14.0.2.1
- ControlsFX 11.0.2
- JPA 2.2
- Hibernate ORM 5.4.23.Final with HikariCP
- JasperReports 6.18.1
- Jaspersoft Studio 6.14.0
- Apache Commons 3.11
- Logback 1.2.3
- Flyway 6.5.3
- MySQL ver: 8.0.19

Run the CustomersFX Project:
--
1. Clone the CustomersFX project:

    git clone https://gitlab.com/cemikta/customersfx-mvc.git

2. Maven install all dependencies:

    mvn clean install
    
3. Install MySQL if not installed and create **MySQL Database** in terminal or in your favorite MySQL tool: 
    
    CREATE DATABASE customersfx_mvc_db CHARACTER SET utf8 COLLATE utf8_general_ci;
    
4. Change your **MySQL credentials** in pom.xml:
    
     database.user
     
     database.password
            
5. Run CustomersFX MVC
    
    mvn clean javafx:run

CustomersFx Demo Accounts:
--
Username: admin

Password: 1

Username: editor

Password: 1