INSERT INTO app_user (app_user_id, username, password, name, app_user_role, active, items_per_page,
  show_message_dialog, show_info_popups, show_tip_of_the_day, app_theme, created_by, created_at)
VALUES
  (1, 'admin', '1', 'Admin User', 'ADMIN', true, 20, true, true, true, 'Light', 1, '2016-09-24 23:57:20'),
  (2, 'editor', '1', 'Editor User', 'EDITOR', true, 20, true, true, true, 'Light', 1, '2016-09-24 23:57:21');