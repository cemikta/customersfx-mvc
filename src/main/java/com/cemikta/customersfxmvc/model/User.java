/*
 * Copyright (C) 2020 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.cemikta.customersfxmvc.model;

import javax.persistence.*;

/**
 * User entity
 *
 * @author Cem Ikta
 */
@Entity
@Table(name = "app_user", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username"})
})
@NamedQueries({
        @NamedQuery(name = User.FIND_ALL,
                query = "SELECT u FROM User u ORDER BY u.name"),
        @NamedQuery(name = User.FIND_BY_USERNAME,
                query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = User.FIND_BY_FILTER,
                query = "SELECT u FROM User u WHERE u.username LIKE :search OR u.name LIKE :search ORDER BY u.name")
})
@AttributeOverride(name = "id", column = @Column(name = "app_user_id", nullable = false,
        columnDefinition = "BIGINT UNSIGNED"))
public class User extends BaseEntity {

    public static final String FIND_ALL = "User.findAll";
    public static final String FIND_BY_USERNAME = "User.findByUsername";
    public static final String FIND_BY_FILTER = "User.findByFilter";

    @Column(name = "username", nullable = false, length = 20)
    private String username;

    @Column(name = "password", nullable = false, length = 20)
    private String password;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "app_user_role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Lob
    @Column(name = "notes", length = 65535, columnDefinition = "TEXT")
    private String notes;

    @Column(name = "items_per_page", nullable = false)
    private Integer itemsPerPage;

    @Column(name = "show_message_dialog", nullable = false)
    private Boolean showMessageDialog;

    @Column(name = "show_info_popups", nullable = false)
    private Boolean showInfoPopups;

    @Column(name = "show_tip_of_the_day", nullable = false)
    private Boolean showTipOfTheDay;

    @Column(name = "report_export_directory", length = 255)
    private String reportExportDirectory;

    @Column(name = "app_theme", length = 50, nullable = false)
    private String appTheme;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Boolean isShowMessageDialog() {
        return showMessageDialog;
    }

    public void setShowMessageDialog(Boolean showMessageDialog) {
        this.showMessageDialog = showMessageDialog;
    }

    public Boolean isShowInfoPopups() {
        return showInfoPopups;
    }

    public void setShowInfoPopups(Boolean showInfoPopups) {
        this.showInfoPopups = showInfoPopups;
    }

    public Boolean isShowTipOfTheDay() {
        return showTipOfTheDay;
    }

    public void setShowTipOfTheDay(Boolean showTipOfTheDay) {
        this.showTipOfTheDay = showTipOfTheDay;
    }

    public String getReportExportDirectory() {
        return reportExportDirectory;
    }

    public void setReportExportDirectory(String reportExportDirectory) {
        this.reportExportDirectory = reportExportDirectory;
    }

    public String getAppTheme() {
        return appTheme;
    }

    public void setAppTheme(String appTheme) {
        this.appTheme = appTheme;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
