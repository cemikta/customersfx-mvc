/*
 * Copyright (C) 2020 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.cemikta.customersfxmvc.util;

import com.cemikta.customersfxmvc.control.IntegerField;
import com.cemikta.customersfxmvc.control.PasswordFieldExt;
import com.cemikta.customersfxmvc.control.ShortField;
import com.cemikta.customersfxmvc.control.TextFieldExt;
import com.cemikta.customersfxmvc.control.fonticon.FontIcon;
import com.cemikta.customersfxmvc.control.fonticon.FontIconFactory;
import com.cemikta.customersfxmvc.control.fonticon.FontIconSize;
import javafx.scene.control.*;
import org.apache.commons.lang3.StringUtils;

/**
 * View helpers
 *
 * @author Cem Ikta
 */
public class ViewHelpers {

    public static Label createFontIconLabel(FontIcon fontIcon, String tooltip) {
        return createFontIconLabel(null, fontIcon, tooltip);
    }

    public static Label createFontIconLabel(String text, FontIcon fontIcon, String tooltip) {
        Label label = new Label();
        label.setText(text);
        label.setGraphic(FontIconFactory.createIcon(fontIcon));
        if (StringUtils.isNotEmpty(tooltip)) {
            label.setTooltip(new Tooltip(tooltip));
        }
        return label;
    }

    public static Button createFontIconButton(String title, FontIcon fontIcon, ContentDisplay contentDisplay) {
        return createFontIconButton(title, fontIcon, FontIconSize.SM, contentDisplay);
    }

    public static Button createFontIconButton(String title, FontIcon fontIcon, FontIconSize fontIconSize,
                                              ContentDisplay contentDisplay) {
        Button btn = new Button(title, FontIconFactory.createIcon(fontIcon, fontIconSize));
        btn.setContentDisplay(contentDisplay);
        return btn;
    }

    private static void setTextInputWidth(TextInputControl textInputControl, double prefWidth) {
        textInputControl.setPrefWidth(prefWidth);
        textInputControl.setMinWidth(prefWidth);
        textInputControl.setMaxWidth(prefWidth);
    }

    public static TextField createTextField(double prefWidth) {
        TextField textField = new TextField();
        setTextInputWidth(textField, prefWidth);
        return textField;
    }

    public static TextFieldExt createTextFieldExt(int length, double prefWidth) {
        TextFieldExt textFieldExt = new TextFieldExt(length);
        setTextInputWidth(textFieldExt, prefWidth);
        return textFieldExt;
    }

    public static PasswordField createPasswordField(double prefWidth) {
        PasswordField passwordField = new PasswordField();
        setTextInputWidth(passwordField, prefWidth);
        return passwordField;
    }

    public static PasswordFieldExt createPasswordFieldExt(int length, double prefWidth) {
        PasswordFieldExt passwordFieldExt = new PasswordFieldExt(length);
        setTextInputWidth(passwordFieldExt, prefWidth);
        return passwordFieldExt;
    }

    public static ShortField createShortField(double prefWidth) {
        ShortField shortField = new ShortField();
        setTextInputWidth(shortField, prefWidth);
        return shortField;
    }

    public static IntegerField createIntegerField(double prefWidth) {
        IntegerField integerField = new IntegerField();
        setTextInputWidth(integerField, prefWidth);
        return integerField;
    }

    public static TextArea createTextArea(double prefWidth, int prefRowCount) {
        TextArea textArea = new TextArea();
        setTextInputWidth(textArea, prefWidth);
        textArea.setPrefRowCount(prefRowCount);
        return textArea;
    }

}
