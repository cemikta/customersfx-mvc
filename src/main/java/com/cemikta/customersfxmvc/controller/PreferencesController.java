/*
 * Copyright (C) 2020 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.cemikta.customersfxmvc.controller;

import com.cemikta.customersfxmvc.CustomersFxApp;
import com.cemikta.customersfxmvc.app.AppFeatures;
import com.cemikta.customersfxmvc.app.AppTheme;
import com.cemikta.customersfxmvc.control.MessageBox;
import com.cemikta.customersfxmvc.control.TipOfTheDay;
import com.cemikta.customersfxmvc.model.User;
import com.cemikta.customersfxmvc.mvc.Controller;
import com.cemikta.customersfxmvc.service.UserService;
import com.cemikta.customersfxmvc.util.I18n;
import com.cemikta.customersfxmvc.view.PreferencesDialog;
import com.cemikta.customersfxmvc.view.ThemesDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * User Preferences controller
 *
 * @author Cem Ikta
 */
public class PreferencesController implements Controller {

    private final Logger logger = LoggerFactory.getLogger(PreferencesController.class);
    private User currentUser;
    private UserService userService;

    public PreferencesController() {
        userService = new UserService();
        // refresh current user
        currentUser = userService.find(CustomersFxApp.get().getCurrentUser().getId());
    }

    public void showPreferences() {
        new PreferencesDialog(CustomersFxApp.get().getMainStage(), this, currentUser).showAndWait();
    }

    public void showThemes() {
        new ThemesDialog(CustomersFxApp.get().getMainStage(), this, currentUser).showAndWait();
    }

    public void showTipOfTheDay() {
        TipOfTheDay tipOfTheDay = new TipOfTheDay(CustomersFxApp.get().getMainStage(), getTipsProperties());
        tipOfTheDay.getShowOnStartupCheckBox().setSelected(currentUser.isShowTipOfTheDay());
        tipOfTheDay.showAndWait();
        if (currentUser.isShowTipOfTheDay() != tipOfTheDay.getShowOnStartupCheckBox().isSelected()) {
            currentUser.setShowTipOfTheDay(tipOfTheDay.getShowOnStartupCheckBox().isSelected());
            onSave(currentUser);
        }
    }

    private String getTipsProperties() {
        String locale = Locale.getDefault().toString();
        switch (locale) {
            case "en_GB":
                return AppFeatures.TIPS_PROPERTIES;
            case "de_DE":
                return AppFeatures.TIPS_DE_PROPERTIES;
            case "tr_TR":
                return AppFeatures.TIPS_TR_PROPERTIES;
            default:
                throw new IllegalArgumentException("Tips of the locale not found!");
        }
    }

    public void onSave(User entity) {
        logger.info("on save action");
        try {
            entity.setUpdatedBy(CustomersFxApp.get().getCurrentUser().getId());
            currentUser = userService.update(entity);
            CustomersFxApp.get().setCurrentUser(currentUser);

        } catch (Exception e) {
            logger.error("Preferences save error", e);
            MessageBox.create()
                    .owner(CustomersFxApp.get().getMainStage())
                    .contentText(I18n.COMMON.getString("notification.saveException"))
                    .showError(e);
        }
    }

    public void changeAppTheme(AppTheme appTheme) {
        CustomersFxApp.get().changeAppTheme(appTheme);
    }

    @Override
    public String getName() {
        return "PreferencesController";
    }

}
